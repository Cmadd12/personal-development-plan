# Python Notes

1. What is a function?
    - can have an effect print() writes output to the editor.
    - can have a result sum(x)
2. Another important part of functions is the "arguements" that a function can take
    - Can be one arguement, many or even 0!
3. Helpful how " " assists interpreter to process strings.
    - You can imagine that the quotes say something like: the text between us is not code. It isn’t intended to be executed, and you should take it as is.
    - Almost anything you put inside the quotes will be taken literally, not as code, but as data.
4. Instructions
    - Python’s syntax is quite specific in this area. Unlike most programming languages, Python requires that there cannot be more than one instruction in a line.
    - A line can be empty (i.e., it may contain no instruction at all) but it must not contain two, three or more instructions. This is strictly prohibited.
    - Each print() invocation contains a different string, as its argument and the console content reflects it – this means that the instructions in the code are executed in the same order in which they have been placed in the source file; no next instruction is executed until the previous one is completed (there are some exceptions to this rule, but you can ignore them for now)
5. Escape character \
    - The word escape should be understood specifically – it means that the series of characters in the string escapes for the moment (a very short moment) to introduce a special inclusion.
    - In other words, the backslash doesn’t mean anything in itself, but is only a kind of announcement, that the next character after the backslash has a different meaning too.
6. Print()
    - a print() function invoked with more than one argument outputs them all on one line
    - the print() function puts a space between the outputted arguments on its own initiative
7. Positional approach
    - print('My name is','Python',end=" ")
    - print('Monty Python')
    - a keyword argument consists of three elements: a keyword identifying the argument (end here); an equal sign (=); and a value assigned to that argument;
    - any keyword arguments have to be put after the last positional argument (this is very important)
    - use sep to influence delimeter print('My','name','is','Monty','Python',sep='-')

'''
print('Pussy cat, pussy cat\nwhere have you been?')
print()
print("I've been to London\nto look at the queen")
print('Pussy cat,' , 'pussy cat' , 'where have you been?')
print('My name is','Python',end=" ")
print('Monty Python')

print('My name is',end='\n')
print('Monty Python')

print('My','name','is','Monty','Python',sep=' ')

print('My','name','is',sep='_',end='*')
print('Monty','Python',sep='*',end='*\n')
'''

8. A literal is data whose values are determined by the literal itself.
    - 123 is a literal
    - is c a literal? WHO KNOWs
    - WHO KNOWs is the clue – 123 is a literal, and c is not.

9. Numbers
    - Integers, that is, those which are devoid of the fractional part;
    - Floating-point numbers (or simply floats), that contain (or are able to contain) the fractional part.
    - It’s clear that this provision makes it easier to read, especially when the number consists of many digits. However, Python doesn’t accept things like these. It’s prohibited. What Python does allow, though, is the use of underscores in numeric literals.
    - How do we code negative num print(-111_111)
    - Positive numbers do not need to be preceded by the plus sign, but it’s permissible, if you wish to do it. pint(+111_111)

10. The first allows us to use numbers in an octal representation.
    - If an integer number is preceded by an 0O or 0o prefix (zero-o), it will be treated as an octal value. 
    - This means that the number must contain digits taken from the [0..7] range only.
        -  print(0o123) or print(0O123)= 83
    - The second allows us to use hexadecimal numbers. Such numbers should be preceded by the prefix 0x or 0X (zero-x).
    - 0x123 is a hexadecimal number with a (decimal) value equal to 291.
        - print(0x123) 291
11. Floating points
    - But don’t forget this simple rule – you can omit zero when it is the only digit in front of or after the decimal point.
        - print(.4)
        - print(4.)
        - 4 is an integer number
        - 4.0 is a floating-point number

12. When you want to use any numbers that are very large or very small, you can use scientific notation.
    - 3E4 = 30000.0
    - The letter E (you can also use the lower-case letter e – it comes from the word exponent) is a concise record of the phrase times ten to the power of.
    - A physical constant called Planck’s constant (and denoted as h), according to the textbooks, has the value of 6.62607 × 10−34
    - print(6.62607E-34)
    - Python always chooses the more economical form of the number’s presentation, and you should take this into consideration when creating literals.
    
13. Strings with " " inside
    - print('I like "MonthyPython"') This will work!
    - print("I like "MonthyPython"") This will error!
    - print("I like \"MonthyPython\"") This will work!
    
14. An operator is a symbol of the programming language, which is able to operate on the values.
    - Data and operators when connected together form expressions. The simplest expression is a literal itself.
    - A ** (double asterisk) sign is an exponentiation (power) operator.
        - Its left argument is the base, its right, the exponent.
        - print(2 ** 3)
    -  It’s possible to formulate the following rules based on this result:
        -  when both ** arguments are integers, the result is an integer, too;
        -  when at least one ** argument is a float, the result is a float, too.
    - An * (asterisk) sign is a multiplication operator.
    - A / (slash) sign is a divisional operator. The value in front of the slash is a dividend, the value behind the slash, a divisor.
    - The result produced by the division operator is always a float, regardless of whether or not the result seems to be a float at first glance:
        - Division is ALWAYS a float.
    -A // (double slash) sign is an integer divisional operator. It differs from the standard / operator in two details:
        - Its result lacks the fractional part – it’s absent (for integers), or is always equal to zero (for floats); this means that the results are always rounded;
        - print(6 // 3) provides a integer result all other cases are a float.
        - print(6 / 4) keeps decimal
        - print(6 // 4) Rounds down This is very important – rounding always goes to the lesser integer.
    - The next operator is quite a peculiar one, because it has no equivalent among traditional arithmetic operators.
        - Its graphical representation in Python is the % (percent) sign, which may look a bit confusing.
        - Try to think of it as of a slash (division operator) accompanied by two funny little circles.
        - The next operator is quite a peculiar one, because it has no equivalent among traditional arithmetic operators.
        - Its graphical representation in Python is the % (percent) sign, which may look a bit confusing.
        - Try to think of it as of a slash (division operator) accompanied by two funny little circles.
        - Note: the operator is sometimes called modulo in other programming languages. print(14 % 2)
15. As you probably know, division by zero doesn’t work. Do not try to:
        - perform a division by zero;
        - perform an integer division by zero;
        - find a remainder of a division by zero
16. In subtracting applications, the minus operator expects two arguments: the left (a minuend in arithmetical terms) and right (a subtrahend).
17. You probably remember from school that multiplications precede additions.
        - You surely remember that you should first multiply 3 by 5 and, keeping the 15 in your memory, then add it to 2, thus getting the result of 17.
        - The phenomenon that causes some operators to act before others is known as the hierarchy of priorities.
        - Python precisely defines the priorities of all operators, and assumes that operators of a larger (higher) priority perform their operations before the operators of a lower priority.
18. The binding of the operator determines the order of computations performed by some operators with equal priority, put side by side in one expression.
    - Most of Python’s operators have left-sided binding, which means that the calculation of the expression is conducted from left to right.
19. The result clearly shows that the exponentiation operator uses right-sided binding.
    - print(2 ** 2 ** 3)
20. Operators in lowest to highest priorities:
    -    + - is unary
    -    **
    -    */%
    -    + - binary
21. How do you save the intermediate results, and use them again to produce subsequent ones?
    - Python will help you with that. It offers special “boxes” (containers) for that purpose, and these boxes are called variables – the name itself suggests that the content of these containers can be varied in (almost) any way.
    - This is worth remembering.
    - What does every Python variable have?
        -  a name;
        -  a value (the content of the container)
22. If you want to give a name to a variable, you must follow some strict rules:
    - the name of the variable must be composed of upper-case or lower-case letters, digits, and the character _ (underscore)
    - the name of the variable must begin with a letter;
    - the underscore character is a letter;
    - upper- and lower-case letters are treated as different (a little differently than in the real world – Alice and ALICE are the same first names, but in Python they are two different variable names, and consequently, two different variables);
    - the name of the variable must not be any of Python’s reserved words (the keywords – we’ll explain more about this soon).
23. A remark inserted into the program, which is omitted at runtime, is called a comment.
    - Whenever Python encounters a comment in your program, the comment is completely transparent to it – from Python’s point of view, this is only one space (regardless of how long the real comment is).
24. Describing a varialbe in a appropriate way is called: self-commenting.
25. Shortcut operators!
    - x = x*2 would be x*=2  and sheep = sheep + 1 would be sheep +=1 
    - If op is a two-argument operator (this is a very important condition) and the operator is used in the following context:
        - variable = variable op expression;
    - It can be simplified and shown as follows:
        - variable op= expression;
'''
x = 15
y = 8

#the shorthand of x = x + y:
x+= y
print(x)

#the shorthand of x = x - y:
x-= y
print(x)

#the shorthand of x = x % y:
x%= y
print(x)

#the shorthand of y = y ** y:
y**= y
print(y)
'''
26. Input!
    - Key point -  the result of the input() function is a string. 

'''
print('Tell me anything...')
anything = input()
print('Hm..', anything, 'Really!?')
'''

- even more elegant solution:
'''
anything = input('Tell me anything...')
print('Hm..', anything, 'Really!?')
'''
27. How to take a string and change it into a number
'''
anything = float(input('Give me a mnumner:'))
something = anything ** 2.0
print(anything, "to the power of 2 is", something)
'''

28. +* in relation to strings!
    - The + (plus) sign, when applied to two strings, becomes a concatenation operator
    - The * (asterisk) sign, when applied to a string and number (or a number and string, as it remains commutative in this position) becomes a replication operator
        - 5 * "2" yields "22222" not 10! 
    - Note: a number less than or equal to zero produces an empty string.

29. lets draw a rectangle!
print("+" + 10*"-"+ "+")
print(("|" +''*10+'|\n')*5,end="")
print("+"+10*'_'+'+')


- Module 1 Quiz

### Assignment:
- Don’t forget this important distinction:

    = is an assignment operator (a = b assigns a with the value of b)

    == is the question are these values equal? (a == b compares a and b)

    t is a binary operator with left-sided binding. It needs two arguments and checks if they are equal.


- What can you do with the answer you get from the computer?

- There are at least two possibilities: first, you can memorize it (store it in a variable) and make use of it later. How do you do that?

    CurrentSpeed > 85



- It’s just like in real life: you do certain things or you don’t when a specific condition is met or not, e.g., you go for a walk if the weather is good, or stay home if it’s wet and cold.

- To make such decisions, Python offers a special instruction. Due to its nature and its application, it’s called a conditional instruction (or conditional statement).

- This conditional statement consists of the following, strictly necessary, elements in this and this order only:

    the if keyword;
    
    one or more white spaces;
    
    an expression (a question or an answer) whose value will be interpreted solely in terms of True (when its value is non-zero) and False (when it is equal to zero);
    
    a colon followed by a newline;

- An intended instruction or instructions (at least one instruction is absolutely required);
    the indentation may be achieved in two ways – by inserting a particular number of spaces, or by using the tab character (the latter way is recommended)
    ; note: if there is more than one instruction in the indented part, the indentation should be the same in all lines
    ; it may look the same if you use tabs mixed with spaces, but it’s important to make all indentations exactly the same)

- How does that statement work?

    if true_or_not:
        do_this_if_true

- If the true_or_not expression represents the truth (i.e., its value is not equal to zero), the indented statement(s) will be executed;

- If the true_or_not expression does not represent the truth (i.e., its value is equal to zero), the indented statement(s) will be omitted (ignored), and the next executed instruction will be the one after the original indentation level.


- In real life, we often express a desire:

    If the weather is good, we’ll go for a walk

    then, we’ll have lunch

 

- As you can see, having lunch is not a conditional activity and doesn’t depend on the weather.

- Knowing what conditions influence our behavior, and assuming that we have the parameterless functions GoForAWalk() and HaveLunch(),

    if TheWeatherIsGood:
        GoForAWalk()
    HaveLunch()


- If a particular sleepless Python developer falls asleep when he or she counts 120 sheep, and the sleep-inducing procedure may be implemented as a special function named SleepAndDream(), the whole code takes the following shape →

 

- You can read it as: if SheepCounter is greater than or equal to 120, then fall asleep and dream.

    ff SheepCounter >=120:
        SleepAndDream()

- We’ve said that conditionally executed statements have to be indented. This creates a very legible structure, clearly demonstrating all possible execution paths in the code.


- Thus, there is a new word: else – this is a keyword.

- The part of the code which begins with else says what to do if the condition specified for the if is not met (note the colon after the word).

- The if-else execution goes as follows:

    if the condition evaluates to True (its value is not equal to zero), the perform_if_condition_true statement is executed, and the conditional statement comes to an end;
    
    if the condition evaluates to False (it is equal to zero), the perform_if_condition_false statement is executed, and the conditional statement comes to an end.
    
    if TheWeatherIsGood:
        GoForAWalk()
    else:
        GoToATheatre()
    HaveLunch()


- Read what we have planned for this Sunday. If the weather is fine, we’ll go for a walk. If we find a nice restaurant, we’ll have lunch there. Otherwise, we’ll eat a sandwich. If the weather is poor, we’ll go to the theater. If there are no tickets, we’ll go shopping in the nearest mall.

    if TheWeatherIsGood:
        if NiceRestaurantFound:
            HaveLunch()
        else:
            EatASandwhich()
    else:
        if TicketsAvailable:
            GoToTheTheatre()
        else:
            GoShopping()
        

- Our next example resembles nesting, but the similarities are very slight. Again, we’ll change our plans and express them as follows: If the weather is fine, we’ll go for a walk, otherwise if we get tickets, we’ll go to the theater, otherwise if there are free tables at the restaurant, we’ll go for lunch; if all else fails, we’ll return home and play chess.

- Have you noticed how many times we’ve used the word “otherwise”? This is the stage where the elif keyword plays its role.

    if TheWeatherIsGood:
        GoForAWalk()
    elif TicketsAvailable:
        GoToAThreatre()
    elif TableAvailable:
        GoForLunch()
    else:
        PlayChessAtHome()

- The way to assemble subsequent if-elif-else statements is sometimes called a cascade.

- Notice again how the indentation improves the readability of the code.

 
- Some additional attention has to be paid in this case:

    you mustn’t use else without a preceding if;
    
    else is always the last branch of the cascade, regardless of whether you’ve used elif or not;
    
    else is an optional part of the cascade, and may be omitted;
    
    if there is an else branch in the cascade, only one of all the branches is executed;
    
    if there is no else branch, it’s possible than none of the available branches is executed.
 

- This may sound a little puzzling, but hopefully some simple examples will help shed more light.

    #read two numbers
    number1 = int(input("Enter the first number"))
    number2 = int(input("Enter the second number"))
    
    #choose the larger number
    if number1 > number2:
        max = number1
    else:
        max = number2
    print("The larger numbner is",max)
    
    Now this example takes three!
    
    #read two numbers
    number1 = int(input("Enter the first number"))
    number2 = int(input("Enter the two number"))
    number3 = int(input("Enter the three number"))
    
    #we temperarily assume that the first numbers
    # is the largest number
    max = number1
    
    #check if the second value is larger
    if number2 > max:
        max = number2
    
    if number3 > max:
        max = number3
    
    print("The largest number is",max)



- Here are two important points:

    this use of the if statement is known as nesting; remember that every else refers to the if which lies at the same indentation level; you need to know this to determine how the ifs and elses pair up;

    consider how the indentation improves readability, and makes the code easier to understand and trace.




### Loops

- Python loop example: 
    while conditional_exspression:
            instruction
    The semantic difference is more important: when the condition is met, if performs its statements only once; while repeats the execution as long as the condition evaluates to True.

    secret_number = 8
    print("Welcome to my game, muggle!\nEnter an integer number and guess what number I've picked for you!\nI'll give you a hint: it's an integer number from 0 to 10.")

    user_number = int(input("Enter an integer number from 0 to 10: "))

    while user_number != secret_number:
        print("No, that's not the number I've picked for you. Try again!")
        user_number = int(input("Enter an integer number from 0 to 10: "))

    print(secret_number, "Well done! That's the number I've chosen for you! You are free now.")

- for loops
    the for keyword opens the for loop; note – there’s no condition after it; you don’t have to think about conditions, as they’re checked internally, without any intervention;
    any variable after the for keyword is the control variable of the loop; it counts the loop’s turns, and does it automatically;
    the in keyword introduces a syntax element describing the range of possible values being assigned to the control variable;

    for i in range(10):
        print("Value of i in currently", i)
    
    for i in range(2, 8):
        print("Value of i in currently", i)

- The third argument is an increment – it’s a value added to control the variable at every loop turn (as you may suspect, the default value of the increment is 1).
        
    for i in range(2, 8, 3):
        print("Value of i in currently", i)

- Note: the set generated by the range() has to be sorted in ascending order. There’s no way to force the range() to create a set in a different form. This means that the range()’s second argument must be greater than the first.
    for i in range(2,1):
        print("Value of i in currently", i)

- Let’s say for the sake of accuracy that their existence in the language is not necessary – an experienced programmer is able to code any algorithm without these instructions. Such additions, which don’t improve the language’s expressive power, but only simplify the developer’s work, are sometimes called syntactic candy,or syntactic sugar.

- These two instructions are:

- break – exits the loop immediately, and unconditionally ends the loop’s operation; the program begins to execute the nearest instruction after the loop’s body;
    while True:
        word = input("You're stuck in an infinite loop! Enter a secret word to leave the loop: ")
        if word == "chupacabra":
            break
    print("You've successfully left the loop!")

- continue – behaves as if the program has suddenly reached the end of the body; the next turn is started and the condition expression is tested immediately.
    word = input("Enter your word:")
    for letter in word:
        if letter == "a":
            continue
        elif letter == "e":
            continue
        elif letter == "i":
            continue
        elif letter == "o":
            continue
        elif letter == "u":
            continue
        else:
            print(letter)

- the value it had before the loop.

- Note: if the control variable doesn’t exist before the loop starts, it won’t exist when the execution reaches the else branch.
    i = 111
    for i in range(2,1):
        print(i)
    else:
        print("else:", i)

- Logical Operators:

### ANR OR
- If we have some free time, and the weather is good, we will go for a walk.

- We’ve used the conjunction “and”, which means that going for a walk depends on the simultaneous fulfilment of these two conditions. In the language of logic, such a connection of conditions is called a conjunction. And now another example:

- If you are in the mall or I am in the mall, one of us will buy a gift for Mom.

- The appearance of the word “or” means that the purchase depends on at least one of these conditions. In logic, such a compound is called a disjunction.

### NOT
- It’s a unary operator performing a logical negation. Its operation is simple: it turns truth into falsehood and falsehood into truth. This operator is written as the word not, and its priority is very high: the same as the unary +/-.

    i = 1
    j = not not i
    print(j) ==TRUE
    
    
    i = 0
    j = not not i
    print(j) ==FALSE




- Lists:
- The elements inside a list may have different types. Some of them may be integers, others floats, and yet others may be lists.
- list is zero based index

- How to assign a value of existing list:
    
    listtest = [1, 2, 3, 4, 5]
    listtest[0] = 10000
    del(listtest[1])
    #negative indecies help you get las item of the list
    listtest[-1]
    listtest[2] = [4]

    print(listtest)


- Note: all the indices used so far are literals. Their values are fixed at runtime, but any expression can be the index, too. This opens up lots of possibilities.

### Method vs. Functions
- A method does all these things, but is also able to change the state of a selected entity. A method is owned by the data it works for, while a function is owned by the whole code.

- Function:
    result = function(arg)
    Method:
    result = data.method(arg)

- how to append to a list? You append with a method on the list.
    list.append('value')
    list.insert(0, 'value') #will allow you to add to a specific place in a list.

    list = []
    for i in range(5):
        list.append(i+1)
    print(list)
    
    [1, 2, 3, 4, 5]
    
    list = []
    for i in range(5):
        list.insert(0, i+1)
    print(list)
    
    [5, 4, 3, 2, 1]


- the for instruction specifies the variable used to browse the list (i here) followed by the in keyword and the name of the list being processed (list here)
- the i variable is assigned the values of all the subsequent list’s elements, and the process occurs as many times as there are elements in the list;
- this means that you use the i variable as a copy of the elements’ values, and you don’t need to use indices;
- the len() function is not needed here, either.

    list = [10, 1, 8, 3, 5]
    sum = 0
    for i in list:
        sum +=1
    print(sum)
    5

- How to swap postions in a list?
- Here is an elegant solution:
    variable1 = 1
    variable2 = 2

    variable1, variable2 = variable2, variable1

    list = [10, 1, 8, 3, 5]
    list[0], list[4] = list[4], list[0]
    list[1], list[3] = list[3], list[1]
    print(list)
    
    [5, 3, 8, 1, 10]

- How to do it for hundreds with a for loop?

    list = [10, 1, 8, 3, 5]
    l = len(list)
    for i in range(l // 2):
        list[i], list[l - i - 1] = list[l - i - 1], list[i]
    print(list)

    [5, 3, 8, 1, 10]

- a bubble sort sorting method
- python bubble sort fix:

    list = [8, 10, 6, 2, 4]

    swapped = True
    while swapped:
        swapped = False
        for i in range(len(list) - 1):
            if list[i] > list[i + 1]:
                swapped = True
                list[i], list[i + 1] = list[i + 1], list[i]
    print(list)
    
    
    list = []
    
    swapped = True
    num = int(input("How many elements do you want to sort?"))
    for i in range(num):
        val = float(input("Enter next element:"))
        list.append(val)
    while swapped:
        swapped = False
        for i in range(len(list) - 1):
            if list[i] > list[i + 1]:
                swapped = True
                list[i], list[i + 1] = list[i + 1], list[i]
    print("Sorted:")
    print(list)

- REMEBER THIS:
    list1 = [1]
    list2 = list1
    list1[0] = 2
    print(list2)

    outputs 2 not 1!

- Lists (and many other complex Python entities) are stored in different ways than ordinary (scalar) variables.

You could say that:

    - the name of an ordinary variable is the name of its content;
    - the name of a list is the name of a memory location where the list is stored.
    
    - The assignment:

    list2 = list1

- copies the name of the array, not its contents. In effect, the two names (list1 and list2) identify the same location in the computer memory. Modifying one of them affects the other, and vice versa.

- How do you cope with that?

- A slice is an element of Python syntax that allows you to make a brand new copy of a list, or parts of a list.

- It actually copies the list’s contents, not the list’s name.

    example of a slice:
    list = [10, 8, 6, 4, 2]
    new_list = list[1:3]
    print(new_list)
    
    output : [8, 6] #notice how 4 isnt included.

- start is the index of the first element included in the slice;
- end is the index of the first element not included in the slice. Think of END -1 [start:end]

- If the start specifies an element lying further than the one described by the end (from the list’s beginning point of view), the slice will be empty:
    
    list = [10, 8, 6, 4, 2]
    new_list = list[-1:1]
    print(new_list)
    output: []

- Omitting both start and end makes a copy of 

    list = [10, 8, 6, 4, 2]
    new_list = list[:]
    print(new_list)
    
    output: [10, 8, 6, 4, 2]

- Del can delete multiple slice too:
    list = [10, 8, 6, 4, 2]
    del list[1:3]
    print(list)
    output: [10,4,2]

- Removing the slice from the code changes its meaning dramatically.
    list = [10, 8, 6, 4, 2]
    del list
    print(list)

- DELETEs the list as a whole

    Python offers two very powerful operators, able to look through the list in order to check whether a specific value is stored inside the list or not.
    in Boolean
    not in Boolean

    list = [10, 8, 6, 4, 2, 12]
    print(5 in list)
    print(5 not in list)
    print(12 in list)
    
    FALSE
    TRUE
    TRUE

- Lists of lists

- A list comprehension is actually a list, but created on-the-fly during program execution, and is not described statically.

    row = [WHITE_PAWN for i in range(8)]

- The part of the code placed inside the brackets specifies:

- the data to be used to fill the list (WHITE_PAWN)
- the clause specifying how many times the data occurs inside the list (for i in range(8))

- list comprehensions can call eachother
    squares = [x ** 2 for x in range(10)]
    odds = [x for x in squares if x % 2 != 0]
    print(odds)
    [1, 9, 25, 49, 81]


- Python does not limit the depth of list-in-list inclusion. Here you can see an example of a three-dimensional array.

- FUNCTIONS Section

- Start writing functions if:
- if a particular fragment of the code begins to appear in more than one place, consider the possibility of isolating it in the form of a function invoked from the points where the original code was placed before.

- A good and attentive developer divides the code (or more accurately: the problem) into well-isolated pieces, and encodes each of them in the form of a function. This considerably simplifies the work of the program, because each piece of code can be encoded separately, and tested separately. The process described here is often called decomposition.

- Create a function if:
    if a piece of code becomes so large that reading and understating it may cause a problem, consider dividing it into separate, smaller problems, and implement each of them in the form of a separate function. This decomposition continues until you get a set of short functions, easy to understand and test.
 
- Create a function if:
    if you’re going to divide the work among multiple programmers, decompose the problem to allow the product to be implemented as a set of separately written functions packed together in different modules.
 
 In general, functions come from at least three places:

- from Python itself – numerous functions (like print()) are an integral part of Python, and are always available without any additional effort on behalf of the programmer; we call these functions built-in functions;
- from Python’s preinstalled modules – a lot of functions, very useful ones, but used significantly less often than built-in ones, are available in a number of modules installed together with Python; the use of these functions requires some additional steps from the programmer in order to make them fully accessible (we’ll tell you about this in a while);
- directly from your code – you can write your own functions, place them inside your code, and use them freely;
- there is one other possibility, but it’s connected with classes, so we’ll omit it for now.



- Example of an ideal time to use a function:
    
    print("Enter next value")
    a = int(input())
    print("Enter next value")
    b = int(input())
    print("Enter next value")
    c = int(input())

- Now what if boss asked you to change the print to start with Please?

- Here is the correct usage with a function:

    def message():
        print("Enter next value")
    
    message()
    a = int(input())
    message()
    b = int(input())
    message()
    c = int(input())


- strucutre of a function = def functionname():
                                function_body
                                
- Here is the new approach in a function:
    def message():
        print("Enter next value")


- Two very important notes about functions:
- You musnt invoke a functiuon that is not known at the moment of invocation.
- You musnt have a function and a vraible of the same name.


    def function(parameter)

- A parameter is actually a variable, but there are two important factors that make parameters different and special:

- they exist only inside functions in which they have been defined, and the only place where the parameter can be defined is a space between a pair of parentheses in the def statement;
    assigning a value to the parameter is done at the time of the function’s invocation, by specifying the corresponding argument.

- Don’t forget:

    parameters live inside functions (this is their natural environment)
    arguments exist outside functions, and are carriers of values passed to corresponding parameters

- It’s legal, and possible, to have a variable named the same as a function’s parameter.

- The snippet illustrates the phenomenon

- A situation like this activates a mechanism called shadowing:

    parameter x shadows any variable of the same name, but…
    … only inside the function defining the parameter.

    def message(no):
        print("Enter value here:", no)

    no = 1234
    message(1)
    print(no)

- This is an example of the no variable outside of the def being compeltley defferent.

def message(what, no):
    print("Enter", what, "number", no)

message("price", 10)
message("tax", 5)

Enter price number 10
Enter tax number 5


def sum(a, b, c):
    print(a,"+", b, "+", c, "=", a+b+c)
sum(1,2,3)
#or
sum(c=1, a=2, b=3)

Be careful, and beware of mistakes. If you try to pass more than one value to one argument, all you’ll get is a runtime error.

#BUT NOT
sum(3, a=1, b=2)

def introduction(firstname, lastname="Smith"):
    print("Hello, my name is", firstname,lastname,".")

introduction("James", "Doe")
#this defults to SMITH
introduction("Henry")


Using return in Python with a function:

*THis is just using return by itself

When used inside a function, it causes the immediate termination of the function’s execution, and an instant return (hence the name) to the point of invocation.

def fun(explode = False):
    print("Three..")
    print("Two..")
    print("One..")
    if not explode:
        return
    print("BOOM!")
fun()

The second return variant is return extentded with an expression:
the function will evaluate the expression’s value and will return (hence the name once again) it as the function’s result.

def boring_function():
    return 13

x = boring_function()
print("The boring_function returned its result", x)

The boring_function returned its result 13

Note, we’re not being too polite here – the function returns a value, and we ignore it (we don’t use it in any way)

def boring_function():
    print("What boredom..")
    return 13

print("Hi, boring!")
boring_function()
print("Stop whining please!")

Hi, boring!
What boredom…
Stop whining, please!

very curious value
None
print(None + 2) will give error! TypeError: unsupported operand type(s) for +: 'NoneType' and 'int'

Note: None is a keyword.


There only two kinds of circumstances when None can be safely used:

when you assign it to a variable (or return it as a function’s result)

when you compare it with a variable to diagnose its internal state.

Don’t forget this: if a function doesn’t return a certain value using a return expression clause, it is assumed that it implicitly returns None.

def strange(n):
    if(n % 2 == 0):
        return True
print(strange(2))
print(strange(3))

True
None

The first is: may a list be sent to a function as an argument?


Of course it may! Any entity recognizable by Python can play the role of a function argument, although it has to be assured that the function is able to cope with it. 

def sumoflist(l):
    sum = 0
    for el in l:
        sum += el
    return sum

print(sumoflist([5,4,3]))

The second question is: may a list be a function result? 


Yes, of course! Any entity recognizable by Python can be a function result.

def strangelist(n):
    list = []
    for i in range(0,n):
        list.insert(0,i)
    return list

print(strangelist(5))

[4, 3, 2, 1, 0]

SCOPE:
This will fail:
def scope():
    x = 123
scope()
print(x)


In other words, does a variable’s name propagate into a function’s body?

def function():
    print("Do I know that variable", variable)
variable = 1
function()
print(variable)

Do I know that variable? 1
1

The answer is: a variable existing outside a function has a scope inside the functions’ bodies.

Now with a little change lets seet what happens!

def function():
    variable = 2
    print("Do I know that variable", variable)
    
variable = 1
function()
print(variable)

Do I know that variable 2
1

This is becuase: A variable existing outside a function has a scope inside the functions’ bodies, excluding those of them which define a variable of the same name.

 does this mean that a function is not able to modify a variable defined outside it?
 NO
 
 here’s a special Python method which can extend a variable’s scope in a way which includes the functions’ bodies (even if you want not only to read the values, but also to modify them).
 GLOBAL
 
 def function():
    global variable
    variable = 2
    print("Do I know that varaible?", variable)

variable = 1
function()
print(variable)

Do I know that varaible? 2
2

Now let’s find out how the function interacts with its arguments.

def function(n):
    print("I got", n)
    n +=1
    print("I have", n)

variable = 1
function(variable)
print(variable)

I got 1
I have 2
1

example with list:
def function(list):
    print(list)
    list = [0,1]

L = [2,3]
function(L)
print(L)

[2, 3]
[2, 3]

We don’t change the value of the parameter list (we already know it will not affect the argument), but instead modify the list identified by it.

def function(list):
    print(list)
    del list[0]

L = [2,3]
function(L)
print(L)

[2, 3]
[3]


BMI Function

def BMI(weight, height):
    return weight/height**2

print(BMI(52.5, 1.65))

More complexity

def BMI(weight, height):
    if height < 1.0 or height > 2.5 or \
        weight < 20 or weight > 200:
            return None
    return weight / height **2

print(BMI(352.5, 1.65))

- All together now!

    def ftintom(ft,inch=0.0):
        return ft * 0.3048 + inch * 0.0254
    def lbtokg(lb):
        return lb * 0.45359237
    def BMI(weight,height):
        if height < 1.0 or height > 2.5 or \
            weight < 20 or weight > 200:
                return None
        return weight / height **2
    
    print(BMI(height=ftintom(5,7), weight=lbtokg(176)))

- Now lets build a function to name a triangle

    def IsItATriangle(a,b,c):
        return a + b > c and b + c > a and c + a >= b
    
    a=float(input("First side's length?"))
    b=float(input("Second side's length?"))
    c=float(input("Third side's length?"))
    if IsItATriangle(a,b,c):
        print("Congrats- it could be a triangle")
    else:
        print("Sorry not a triangle!")
        
    Fibonacci generator!

- WITHOUT RECURSION

    def Fib(n):
        if n < 1:
            return None
        if n < 3:
            return 1
        el1 = el2 = 1
        sum = 0
        for i in range(3,n+1):
            sum = el1 + el2
            el1,el2 = el2,sum
        return sum
    
    for n in range(1, 10):
        print(n, "->", Fib(n))

- RECURSION

    def Fib(n):
        if n < 1:
            return None
        if n < 3:
            return 1
        return Fib(n - 1) + Fib (n - 2)


- Tuples and Dictionaries









    fnam = input('May I have your first name, please?')
    Inam = input('May I have your last name please')
    print("Thank you!")
    print("Your name is " + fnam + " " + Inam + ".")
    
    
    
    print('Pussy cat, pussy cat\nwhere have you been?')
    print()
    print("I've been to London\nto look at the queen")
    print('Pussy cat,' , 'pussy cat' , 'where have you been?')
    print('My name is','Python',end=" ")
    print('Monty Python')
    
    print('My name is',end='\n')
    print('Monty Python')
    
    print('My','name','is','Monty','Python',sep=' ')
    
    print('My','name','is',sep='_',end='*')
    print('Monty','Python',sep='*',end='*\n')
    
    print("2")
    print(2)
    
    print(11111111)
    print(111_111_111)
    print(-111_111)
    print(0o12)
    
    print(.4)
    print(4.)
    
    print(3E4)
    
    print(6.62607E-34)
    print(0.0000000000000000000001)
    
    print("I'm Monthy Python")
    print('I\'m Monthy Python')









