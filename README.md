<img src="https://i0.wp.com/www.entrepreneuryork.com/wp-content/uploads/2016/09/personal-developt.jpg?zoom=2&fit=550%2C288&ssl=1"
     alt="Markdown Monster icon"
     style="float: center; margin-right: 10px;" />

# Personal Development Plan

Below is a series of tables depicting multiple skillsets essential for data engineering to support ML capabilities. 

#### Python learnings:

|Provider| Python Track |Progress|
|------|------|------|
|Python Institue|PCEP - Certified Entry-Level Python Programmer Certification|[COMPLETE!](https://drive.google.com/file/d/1803KJiJux0B-3MFn9egTa_INIh6dPUey/view)|
|DataCamp|Importing data in python| [COMPLETE!](https://www.datacamp.com/statement-of-accomplishment/course/965700d07442e878ee77ff6ce65e23657d4bdaef)|
|DataCamp|Cleaning data in python| [COMPLETE!](https://www.datacamp.com/statement-of-accomplishment/course/74aefa5dc6dacc4c9d5422eb82ca5ee956fabb53) |
|DataCamp|Programming using python| [COMPLETE!](https://www.datacamp.com/statement-of-accomplishment/course/0ffe768b97fe1a596d509d3ae6becf63ed0e8b34) |
|Personal|Create Automated Data Validator| [COMPLETE!](https://gitlab.com/Cmadd12/datavalidator/blob/master/tokenvalidator.py)|
|DataCamp|Data Types for Data Science|[COMPLETE!](https://www.datacamp.com/statement-of-accomplishment/course/2e2ccd45d26a004d12dd7152af73f449d7423d97)|
|DataCamp|Object-Oriented Programming in Python|[In progress](https://www.datacamp.com/courses/object-oriented-programming-in-python)|
|Personal|Create a Python version of Snake|[COMPLETE!](https://gitlab.com/Cmadd12/classicsnakegame_python)|

#### SQL learnings:

|Provider| SQL Track |Progress|
|------|------|------|
|DataCamp|Intro to SQL for Data Science | [COMPLETE!](https://www.datacamp.com/statement-of-accomplishment/course/d3a8aaddf88601f0961d6cec1f2d98b1dcbe0922) |
|DataCamp|Joining Data in SQL| [In progress](https://www.datacamp.com/courses/joining-data-in-postgresql)|
|DataCamp|Intermediate SQL| [In progress](https://www.datacamp.com/courses/intermediate-sql)|


#### Spark learnings:

|Provider| Spark Track |Progress|
|------|------|------|
|DataCamp|Introduction to PySpark | [COMPLETE!](https://www.datacamp.com/statement-of-accomplishment/course/3c97d6702e02124edef919d738c2f80fc090aec0)|
|DataCamp|Big Data Fundamentals via PySpark| [COMPLETE!](https://www.datacamp.com/statement-of-accomplishment/course/9e7a6fc2b024b790c4dfb3f62c1c37a96ac6257c)|


#### NoSQL learnings:

|Provider| NoSQL Track |Progress|
|------|------|------|
|Personal|Create a MongoDB Database | [COMPLETE!](https://gitlab.com/Cmadd12/mongodb-tutorial)|
|Personal|Create a Cassandra Database | In progress|


#### ML learnings:

|Provider| ML Track |Progress|
|------|------|------|
|Personal|Convolutional Neural Network Name that Animal| [COMPLETE!](https://gitlab.com/Cmadd12/convolutional-neural-network-name-that-animal)|
|Personal|Convolutional Neural Network Classify Hand Written Digits| [COMPLETE!](https://gitlab.com/Cmadd12/convolutional-neural-network-classify-hand-written-digits)|
|AWS|Deep learning in AWS| [COMPLETE!](https://aws.amazon.com/training/course-descriptions/deep-learning/) |
|AWS|Machine Learning: Data Platform Engineer | [In progress](https://aws.amazon.com/training/learning-paths/machine-learning/data-platform-engineer)|
|DataCamp|Deep Learning in Python| [In progress](https://www.datacamp.com/courses/deep-learning-in-python)|
|DataCamp|Convolutional Nueral Networks for Image Processing| [In progress](https://www.datacamp.com/courses/convolutional-neural-networks-for-image-processing)|
|Personal|Classification Decision Tree| [COMPLETE!](https://gitlab.com/Cmadd12/docker-classification-decision-tree-)|

#### Docker learnings:

|Provider| Docker Track |Progress|
|------|------|------|
|ACloudGuru|Docker Fundamentals | [COMPLETE!](https://verify.acloud.guru/E948A03E8F18) |
|ACloudGuru|Docker for DevOps | [In progress!]() |
|ACloudGuru|AWS ECS Scaling Docker | [COMPLETE!](https://verify.acloud.guru/75D58A8FC3D9) |
|Personal|Exploratory Data Analysis Image | [In progress!]() |

#### Kubernetes learnings:

|Provider| Kubernetes Track |Progress|
|------|------|------|
|ACloudGuru|Kubernetes | [COMPLETE!](https://verify.acloud.guru/9629EBE62F10) |

#### AWS learnings:

|Provider| AWS Track |Progress|
|------|------|------|
|ACloudGuru|Cloud Practitioner|[COMPLETE!](https://verify.acloud.guru/A9C0A0D11F86)|
|AWS|Cloud Practitioner Certification | [COMPLETE!](https://www.certmetrics.com/amazon/public/badge.aspx?i=9&t=c&d=2018-12-15&ci=AWS00595867)|
|Personal|Data View Generation Innovation| [COMPLETE!](https://gitlab.com/Cmadd12/aws-research-glue-prep)|
|AWS|Big Data Specialty Certification | [In progress](https://aws.amazon.com/certification/certified-big-data-specialty/)|


#### GCP learnings:

|Provider| GCP Track |Progress|
|------|------|------|
|GCP|Google Cloud Platform Fundamentals: Core Infrastructure | [COMPLETE!](https://cloud.google.com/training/courses/core-fundamentals) |
|GCP|Data Engineering on Google Cloud Platform | [In progress](https://cloud.google.com/training/courses/data-engineering)|
