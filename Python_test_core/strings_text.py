#Helpful tutorial



#printed out hello world
message = "Hello World" # can also use ' qutes'
print(message)

#descriptive variables. Help a lot. For example M_ wouldnt be good.
message1 = 'Bobby\'s World' #escape example
message2 = "Bobby's World" #escape example


print(message1)
print(message2)


#multi line string
message3 = '''Was a good
cartoon in the 90's'''
print(message3)



#accessing string contnets
#find length
message = "Hello World" #
print(len(message))
print(message[0])
print(message[0:-1])

#to get the total index's of a string it is total length minus 1 due to zero based indexing
print(message[0:10]) #first index is included. Seoncd index is NOT INDCLUDED.
#example of saying this I want every character except the last that I am calling out.

print(message[10]) #access last position

#THIS IS CALLED SLICING
#Now lets grab just "Hello"
print(message[:5])

#Now lets grab just "World"
print(message[6:])

#Upper case and lower case
print(message.lower())
print(message.upper())
#count string
print(message.count('Hello'))
print(message.count('l'))

#find
print(message.find('World'))
print(message.find('Universe')) #cant find is a -1

#replace
message = message.replace('World', 'Universe')
print(message)


greeting = 'Hello'
name = 'Collin'
message_cat = greeting + ', ' + name + '. Welcome!'

#formateed string
message_cat1 = '{}, {}. Welcome!'.format(greeting, name)

#f-strings
message_cat2 = f"{greeting}, {name.upper()}. Welcome!"

print(message_cat2)
print(dir(name)) #this is a list of everything you can do against the string.
print(help(str))



#Quiz - Python Strings and Character Data

s = 'foo'
t = 'bar'
print('barf' in 2 * (s + t))

#The + operator concatenates strings and the * operator creates multiple copies. 
#The result of 2 * (s + t) is 'foobarfoobar', which does contain the string 'barf'.

###TRUE


print(ord('foo'))

#The ord() function returns the integer value for a given character. 
#But you can only specify a single character (a string of length 1):

"It raises an exception"

print(ord('f'))
#102


#What is the slice expression that gives every third character of string s, 
#starting with the last character and proceeding backward to the first?

# The third index in the slice expression is -3, which indicates every third character stepping backward.

# The first and second indices should be -1 (the last character) and 0 (the first character). Either of these can be allowed to default.

# Any of these expressions will produce the correct answer:
# s[::-3]
# s[-1::-3]
# s[:0:-3]
#s[-1:0:-3]




